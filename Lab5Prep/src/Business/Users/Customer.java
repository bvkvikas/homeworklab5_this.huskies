/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import java.util.Date;

/**
 *
 * @author harshalneelkamal
 */
public class Customer extends User{
    public Customer(String password, String userName, String dateCreated){
        super(password,userName,"Customer",dateCreated);
        System.out.println("Customer panel in customer");
    }
    public boolean verify(String password){
        if(password.equals(getPassword())){
            return true;
        }
        else{
            return false;
        }
    }
    public String getRole(){
        return "Customer";
    }
    
    @Override
    public String toString(){
        return getUserName();
    }
}
