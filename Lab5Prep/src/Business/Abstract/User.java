/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Abstract;

/**
 *
 * @author bvkv
 */
public abstract class User {
    private String password;
    private String userName;
    private String role;
    private String dateCreated;

    public User(String password, String userName, String role, String dateCreated) {
        
        this.password = password;
        this.userName = userName;
        this.role = role;
        this.dateCreated = dateCreated;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        
        this.userName = userName;
    }
    
    abstract public boolean verify(String password);

    public String getRole() {
        return role;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    
    @Override
    public String toString() {
        return getUserName();
    }
    
}
